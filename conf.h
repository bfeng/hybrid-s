#ifndef _CONF_H_
#define _CONF_H_

#include <stdbool.h>

typedef struct {
  char          filename[256];
  bool          debug;

  unsigned int  time_latency;
  unsigned int  client_number;
  unsigned int  server_number;
  unsigned long stripe_size;

  unsigned long FOPEN_init;
  unsigned long FOPEN_terminate;
  unsigned long FOPEN_prelude;
  unsigned long FOPEN_positive_ack;
  unsigned long FOPEN_start_flow;
  unsigned long FOPEN_completion_ack;

  unsigned long FREAD_init;
  unsigned long FREAD_terminate;
  unsigned long FREAD_prelude;
  unsigned long FREAD_positive_ack;
  unsigned long FREAD_start_flow;
  unsigned long FREAD_completion_ack;

  unsigned long FWRITE_init;
  unsigned long FWRITE_terminate;
  unsigned long FWRITE_prelude;
  unsigned long FWRITE_positive_ack;
  unsigned long FWRITE_start_flow;
  unsigned long FWRITE_completion_ack;

  unsigned long FCLOSE_init;
  unsigned long FCLOSE_terminate;
  unsigned long FCLOSE_prelude;
  unsigned long FCLOSE_positive_ack;
  unsigned long FCLOSE_start_flow;
  unsigned long FCLOSE_completion_ack;
} runtime_conf_t;

int load_configuration_file(runtime_conf_t * );

runtime_conf_t runtime_config;

#endif
