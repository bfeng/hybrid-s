#include "conf.h"
#include "model.h"

extern runtime_conf_t runtime_config;

void printf_msg(const char * side, const char * m_type, const message * msg, const tw_lp * lp)
{
    if(runtime_config.debug)
        printf("%s:%u <- %u:%-40s fh: %10u <- %u\n",
                side, (unsigned int)lp->gid, (unsigned int)msg->sender_id,
                m_type, msg->file_handler, (unsigned int)msg->client_id);
}

void send_msg(tw_lpid to, message_type m_type, unsigned int file_handler, tw_stime ts, tw_lp * sender, tw_lpid client_id, unsigned long long offset, unsigned long long length)
{
    tw_event *e;
    message *m;
    e = tw_event_new(to, (double)(ts + runtime_config.time_latency), sender);
    m = tw_event_data(e);
    m->m_type = m_type;
    m->file_handler = file_handler;
    m->sender_id = sender->gid;
    m->offset = offset;
    m->length = length;

    /* ignore the client_id if the caller is the client */
    if(m_type == FOPEN_init ||
            m_type == FREAD_init ||
            m_type == FWRITE_init ||
            m_type == FCLOSE_init) {
        m->client_id = sender->gid;
    }
    else
        m->client_id = client_id;

    if(runtime_config.debug)
    {
        if(m_type == FOPEN_init)
            printf("init request\n");
        if(m_type == FREAD_init || m_type == FWRITE_init)
            printf("original request: [%llu-%llu:%llu]\n", m->offset, m->offset+m->length - 1, m->length);
    }

    tw_event_send(e);
}

void strip_request(client_state *s, tw_lp * lp, message * msg)
{
    int i;
    unsigned long long offset;
    unsigned long long length;

    int first_node;
    int last_node;
    int many_nodes;
    int node_id;
    int logic_id;

    if(msg->m_type == FOPEN_init || msg->m_type == FCLOSE_init)
    {
        first_node = runtime_config.client_number;
        last_node = runtime_config.client_number + runtime_config.server_number - 1;
    }
    else
    {
        first_node = msg->offset / runtime_config.stripe_size % runtime_config.server_number + runtime_config.client_number;
        last_node = (msg->offset + msg->length - 1) / runtime_config.stripe_size + runtime_config.client_number;
    }
    many_nodes = last_node - first_node + 1;
    s->sub_requests = many_nodes;
    if(runtime_config.debug)
    {
        printf("server nodes:[%d]-[%d] -> sub-requests:", first_node, last_node);
    }
    for(i=0;i<many_nodes;++i)
    {
        logic_id = first_node - runtime_config.client_number + i;
        node_id = logic_id % runtime_config.server_number + runtime_config.client_number;

        if(msg->offset > (logic_id * runtime_config.stripe_size))
            offset = msg->offset;
        else
            offset = logic_id * runtime_config.stripe_size;

        if((msg->offset + msg->length - 1) > ((logic_id+1)*runtime_config.stripe_size - 1))
            length = (logic_id+1)*runtime_config.stripe_size - offset;
        else
            length = (msg->offset + msg->length - 1) - offset + 1;

        if(runtime_config.debug)
            printf("[%d|%d:%llu-%llu]", node_id, logic_id, offset, offset+length - 1);

        /* send the message to the server node */
        switch(msg->m_type) {
            case FREAD_init:
                send_msg(node_id, FREAD_prelude, msg->file_handler, runtime_config.FREAD_prelude, lp, msg->client_id, offset, length);
                break;
            case FWRITE_init:
                send_msg(node_id, FWRITE_prelude, msg->file_handler, runtime_config.FWRITE_prelude, lp, msg->client_id, offset, length);
                break;
        }
    }
    if(runtime_config.debug)
        printf("\n");
}

void hpis3_print_stat()
{
    if (tw_ismaster()) {
        puts("------------------------------------------------------------------------------");
        printf("HPIS3 Model Statistics:\n");
    }
    hpis3_print_bw();
    if (tw_ismaster())
        puts("------------------------------------------------------------------------------");
}

void hpis3_print_bw()
{
    int myrank;
    unsigned long long global_size = 0;
    double global_time = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    if (runtime_config.debug)
    {
        printf("rank:%d, local size: %llu\n", myrank, hpis3_local_total_request_size);
        printf("rank:%d, local time: %f\n", myrank, hpis3_local_total_response_time);
    }

    MPI_Reduce(&hpis3_local_total_request_size, &global_size, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&hpis3_local_total_response_time, &global_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    if (tw_ismaster())
    {
        printf("Throughput: %f MB/s\n", global_size/global_time);
    }
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
