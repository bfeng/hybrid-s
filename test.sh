make
for np in 256 128 64 32 16 8 4 2; do
  echo $np
  mpirun -n ${np}	 ./hybrid-s --synch=2 --config=./conf/example2.cfg  > ${np}np.log
  awk '{ if ( $0 ~ /: Running Time/ || $0 ~ /Event Rate/ ) print }' ${np}np.log
done
