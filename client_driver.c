#include "conf.h"
#include "model.h"

extern runtime_conf_t runtime_config;

/* functions for client model */

void client_init(client_state *s, tw_lp *lp)
{
    int i;
    s->open_files = 0;
    s->requests = 1;
    s->sub_requests = 0;
    s->total_request_size = 0;
    s->total_response_time = 0;

    send_msg(lp->gid, FOPEN_init, lp->gid + 1000, runtime_config.FOPEN_init, lp, 0, 0, 0);

    if(runtime_config.debug)
        printf("Client #%u initilized on %u\n", (unsigned int)lp->gid, (unsigned int)g_tw_mynode);
}

void client_event_handler(client_state *s, tw_bf * bf, message * msg, tw_lp * lp)
{
    switch(msg->m_type) {
        case FOPEN_init:
            {
                printf_msg("Client", "FOPEN_init", msg, lp);
                s->open_files++;
                /*printf("msg->client_id:%d\n", msg->client_id);*/
                /*printf("to: %u\n",(unsigned int)runtime_config.client_number + (unsigned int)(lp->gid)%runtime_config.server_number);*/
                send_msg(runtime_config.client_number + (lp->gid)%runtime_config.server_number, FOPEN_prelude, msg->file_handler, runtime_config.FOPEN_prelude, lp, msg->client_id, 0, 0);
                break;
            }
        case FOPEN_terminate:
            {
                printf_msg("Client", "FOPEN_terminate", msg, lp);
                /*send_msg(lp->gid, FREAD_init, lp->gid + 1000, runtime_config.FREAD_init + 1000, lp, 0, (lp->gid)*runtime_config.stripe_size, 100);*/
                send_msg(lp->gid, FREAD_init, lp->gid + 1000, runtime_config.FREAD_init, lp, 0, (lp->gid)*runtime_config.stripe_size, runtime_config.stripe_size);
                break;
            }
        case FREAD_init:
            {
                printf_msg("Client", "FREAD_init", msg, lp);
                s->requests--;
                msg->start_time = tw_now(lp);
                s->total_request_size += msg->length;
                strip_request(s, lp, msg);
                break;
            }
        case FREAD_terminate:
            {
                s->sub_requests--;
                if (s->sub_requests <= 0)
                {
                    printf_msg("Client", "FREAD_terminate", msg, lp);
                    if (s->requests > 0)
                        send_msg(lp->gid, FREAD_init, lp->gid + 1000, runtime_config.FREAD_init, lp, 0, (lp->gid)*runtime_config.stripe_size, 100);
                    if(s->requests <= 0)
                        send_msg(lp->gid, FCLOSE_init, lp->gid + 1000, runtime_config.FCLOSE_init, lp, 0, 0, 0);
                    msg->end_time = tw_now(lp);
                    s->total_response_time += msg->end_time - msg->start_time;
                    if (runtime_config.debug)
                    {
                        printf("time:%f\n", msg->end_time - msg->start_time);
                        printf("%fMB/s\n",  msg->length / (msg->end_time - msg->start_time));
                    }
                }
                break;
            }
        case FWRITE_init:
            {
                printf_msg("Client", "FWRITE_init", msg, lp);
                /*strip_request(lp, msg, runtime_config.FWRITE_init);*/
                break;
            }
        case FWRITE_terminate:
            {
                printf_msg("Client", "FWRITE_terminate", msg, lp);
                break;
            }
        case FCLOSE_init:
            {
                printf_msg("Client", "FCLOSE_init", msg, lp);
                s->open_files--;
                /*strip_request(lp, msg, runtime_config.FCLOSE_init);*/
                send_msg(runtime_config.client_number + (lp->gid)%runtime_config.server_number, FCLOSE_prelude, msg->file_handler, runtime_config.FCLOSE_prelude, lp, msg->client_id, 0, 0);
                break;
            }
        case FCLOSE_terminate:
            {
                printf_msg("Client", "FCLOSE_terminate", msg, lp);
                break;
            }
    }
}

void client_revert_handler(client_state *s, tw_bf * bf, message * msg, tw_lp * lp)
{
}

void client_final(client_state *s, tw_lp *lp)
{
    hpis3_local_total_request_size += s->total_request_size;
    if (s->total_response_time > hpis3_local_total_response_time)
        hpis3_local_total_response_time = s->total_response_time;
    if(runtime_config.debug)
    {
        printf("client#%u: open_files: %d\n", (unsigned int) lp->gid, s->open_files);
        printf("client#%u: requests: %ld\n", (unsigned int) lp->gid, s->requests);
        printf("client#%u: sub_requests: %d\n", (unsigned int) lp->gid, s->sub_requests);
        printf("local size: %llu\n", hpis3_local_total_request_size);
        printf("local time: %f\n", hpis3_local_total_response_time);
    }
}

tw_peid client_mapping(tw_lpid gid)
{
    return (tw_peid) gid / g_tw_nlp;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
