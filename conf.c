#include <mpi.h>
#include <libconfig.h>
#include "conf.h"
#include "ross.h"

int load_configuration_file(runtime_conf_t * p_config)
{
    int i;
    if(tw_ismaster())
    {
        config_t cfg;
        int debug;
        int  time_latency;
        int  num_tmp;
        long long time_tmp;

        config_init(&cfg);

        if(tw_ismaster())
            printf("Loading configuration: %s\n", p_config->filename);
        if(!config_read_file(&cfg, p_config->filename)) {
            if(tw_ismaster())
                fprintf(stderr, "%s:%d - %s\n",
                        config_error_file(&cfg),
                        config_error_line(&cfg),
                        config_error_text(&cfg));
            config_destroy(&cfg);
            return -1;
        }

        if(config_lookup_bool(&cfg, "params.debug", &debug))
        {
            p_config->debug = (bool)debug;
            if(tw_ismaster())
                printf("Debug mode: %s\n", p_config->debug ? "true" : "false");
        }
        else
        {
            p_config->debug = false;
            if(tw_ismaster())
                printf("Debug mode is not defined, settled to false by default\n");
        }

        p_config->stripe_size = 65536;

        if(config_lookup_int(&cfg, "params.latency", &time_latency))
        {
            p_config->time_latency = (unsigned int)time_latency;
            if(tw_ismaster())
                printf("Latency: %u\n", p_config->time_latency);
        }
        else
        {
            p_config->time_latency = 100;
            if(tw_ismaster())
                printf("Latency is not defined, settled to 100 by default\n");
        }

        /* client number */
        if(config_lookup_int(&cfg, "clients.number", &num_tmp))
        {
            p_config->client_number = (unsigned int)num_tmp;
            if(p_config->client_number < 1)
                return -1;
            if(tw_ismaster())
                printf("Clients: %u\n", p_config->client_number);
        }
        else
        {
            p_config->client_number = 1;
            if(tw_ismaster())
                printf("# of clients is not defined, settled to 1 by default\n");
        }

        /* server number */
        if(config_lookup_int(&cfg, "servers.number", &num_tmp))
        {
            p_config->server_number = (unsigned int)num_tmp;
            if(p_config->server_number < 1)
                return -1;
            if(tw_ismaster())
                printf("Servers: %u\n", p_config->server_number);
        }
        else
        {
            p_config->server_number = 1;
            if(tw_ismaster())
                printf("# of servers is not defined, settled to 1 by default\n");
        }

        /* FOPEN */
        if(config_lookup_int64(&cfg, "clients.fopen.init", &time_tmp))
        {
            p_config->FOPEN_init = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fopen.init: %lu\n", p_config->FOPEN_init);
        }
        else
        {
            p_config->FOPEN_init = 100;
            if(tw_ismaster())
                printf("clients.fopen.init is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "clients.fopen.terminate", &time_tmp))
        {
            p_config->FOPEN_terminate = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fopen.terminate: %lu\n", p_config->FOPEN_terminate);
        }
        else
        {
            p_config->FOPEN_terminate = 100;
            if(tw_ismaster())
                printf("clients.fopen.terminate is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fopen.prelude", &time_tmp))
        {
            p_config->FOPEN_prelude = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fopen.prelude: %lu\n", p_config->FOPEN_prelude);
        }
        else
        {
            p_config->FOPEN_prelude = 100;
            if(tw_ismaster())
                printf("servers.fopen.prelude is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fopen.positive_ack", &time_tmp))
        {
            p_config->FOPEN_positive_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fopen.positive_ack: %lu\n", p_config->FOPEN_positive_ack);
        }
        else
        {
            p_config->FOPEN_positive_ack = 100;
            if(tw_ismaster())
                printf("servers.fopen.positive_ack is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fopen.start_flow", &time_tmp))
        {
            p_config->FOPEN_start_flow = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fopen.start_flow: %lu\n", p_config->FOPEN_start_flow);
        }
        else
        {
            p_config->FOPEN_start_flow = 100;
            if(tw_ismaster())
                printf("servers.fopen.start_flow is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fopen.completion_ack", &time_tmp))
        {
            p_config->FOPEN_completion_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fopen.completion_ack: %lu\n", p_config->FOPEN_completion_ack);
        }
        else
        {
            p_config->FOPEN_completion_ack = 100;
            if(tw_ismaster())
                printf("servers.fopen.completion_ack is not defined, settled to 100 by default\n");
        }

        /* FREAD */
        if(config_lookup_int64(&cfg, "clients.fread.init", &time_tmp))
        {
            p_config->FREAD_init = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fread.init: %lu\n", p_config->FREAD_init);
        }
        else
        {
            p_config->FREAD_init = 100;
            if(tw_ismaster())
                printf("clients.fread.init is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "clients.fread.terminate", &time_tmp))
        {
            p_config->FREAD_terminate = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fread.terminate: %lu\n", p_config->FREAD_terminate);
        }
        else
        {
            p_config->FREAD_terminate = 100;
            if(tw_ismaster())
                printf("clients.fread.terminate is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fread.prelude", &time_tmp))
        {
            p_config->FREAD_prelude = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fread.prelude: %lu\n", p_config->FREAD_prelude);
        }
        else
        {
            p_config->FREAD_prelude = 100;
            if(tw_ismaster())
                printf("servers.fread.prelude is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fread.positive_ack", &time_tmp))
        {
            p_config->FREAD_positive_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fread.positive_ack: %lu\n", p_config->FREAD_positive_ack);
        }
        else
        {
            p_config->FREAD_positive_ack = 100;
            if(tw_ismaster())
                printf("servers.fread.positive_ack is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fread.start_flow", &time_tmp))
        {
            p_config->FREAD_start_flow = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fread.start_flow: %lu\n", p_config->FREAD_start_flow);
        }
        else
        {
            p_config->FREAD_start_flow = 100;
            if(tw_ismaster())
                printf("servers.fread.start_flow is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fread.completion_ack", &time_tmp))
        {
            p_config->FREAD_completion_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fread.completion_ack: %lu\n", p_config->FREAD_completion_ack);
        }
        else
        {
            p_config->FREAD_completion_ack = 100;
            if(tw_ismaster())
                printf("servers.fread.completion_ack is not defined, settled to 100 by default\n");
        }

        /* FWRITE */
        if(config_lookup_int64(&cfg, "clients.fwrite.init", &time_tmp))
        {
            p_config->FWRITE_init = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fwrite.init: %lu\n", p_config->FWRITE_init);
        }
        else
        {
            p_config->FWRITE_init = 100;
            if(tw_ismaster())
                printf("clients.fwrite.init is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "clients.fwrite.terminate", &time_tmp))
        {
            p_config->FWRITE_terminate = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fwrite.terminate: %lu\n", p_config->FWRITE_terminate);
        }
        else
        {
            p_config->FWRITE_terminate = 100;
            if(tw_ismaster())
                printf("clients.fwrite.terminate is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fwrite.prelude", &time_tmp))
        {
            p_config->FWRITE_prelude = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fwrite.prelude: %lu\n", p_config->FWRITE_prelude);
        }
        else
        {
            p_config->FWRITE_prelude = 100;
            if(tw_ismaster())
                printf("servers.fwrite.prelude is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fwrite.positive_ack", &time_tmp))
        {
            p_config->FWRITE_positive_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fwrite.positive_ack: %lu\n", p_config->FWRITE_positive_ack);
        }
        else
        {
            p_config->FWRITE_positive_ack = 100;
            if(tw_ismaster())
                printf("servers.fwrite.positive_ack is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fwrite.start_flow", &time_tmp))
        {
            p_config->FWRITE_start_flow = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fwrite.start_flow: %lu\n", p_config->FWRITE_start_flow);
        }
        else
        {
            p_config->FWRITE_start_flow = 100;
            if(tw_ismaster())
                printf("servers.fwrite.start_flow is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fwrite.completion_ack", &time_tmp))
        {
            p_config->FWRITE_completion_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fwrite.completion_ack: %lu\n", p_config->FWRITE_completion_ack);
        }
        else
        {
            p_config->FWRITE_completion_ack = 100;
            if(tw_ismaster())
                printf("servers.fwrite.completion_ack is not defined, settled to 100 by default\n");
        }

        /* FCLOSE */
        if(config_lookup_int64(&cfg, "clients.fclose.init", &time_tmp))
        {
            p_config->FCLOSE_init = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fclose.init: %lu\n", p_config->FCLOSE_init);
        }
        else
        {
            p_config->FCLOSE_init = 100;
            if(tw_ismaster())
                printf("clients.fclose.init is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "clients.fclose.terminate", &time_tmp))
        {
            p_config->FCLOSE_terminate = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("clients.fclose.terminate: %lu\n", p_config->FCLOSE_terminate);
        }
        else
        {
            p_config->FCLOSE_terminate = 100;
            if(tw_ismaster())
                printf("clients.fclose.terminate is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fclose.prelude", &time_tmp))
        {
            p_config->FCLOSE_prelude = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fclose.prelude: %lu\n", p_config->FCLOSE_prelude);
        }
        else
        {
            p_config->FCLOSE_prelude = 100;
            if(tw_ismaster())
                printf("servers.fclose.prelude is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fclose.positive_ack", &time_tmp))
        {
            p_config->FCLOSE_positive_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fclose.positive_ack: %lu\n", p_config->FCLOSE_positive_ack);
        }
        else
        {
            p_config->FCLOSE_positive_ack = 100;
            if(tw_ismaster())
                printf("servers.fclose.positive_ack is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fclose.start_flow", &time_tmp))
        {
            p_config->FCLOSE_start_flow = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fclose.start_flow: %lu\n", p_config->FCLOSE_start_flow);
        }
        else
        {
            p_config->FCLOSE_start_flow = 100;
            if(tw_ismaster())
                printf("servers.fclose.start_flow is not defined, settled to 100 by default\n");
        }

        if(config_lookup_int64(&cfg, "servers.fclose.completion_ack", &time_tmp))
        {
            p_config->FCLOSE_completion_ack = (unsigned long)time_tmp;
            if(tw_ismaster())
                printf("servers.fclose.completion_ack: %lu\n", p_config->FCLOSE_completion_ack);
        }
        else
        {
            p_config->FCLOSE_completion_ack = 100;
            if(tw_ismaster())
                printf("servers.fclose.completion_ack is not defined, settled to 100 by default\n");
        }

        config_destroy(&cfg);
    }

    MPI_Status status;
    MPI_Datatype config_type;
    MPI_Datatype types[30] =
    {
        MPI_CHAR, MPI_C_BOOL,
        MPI_UNSIGNED, MPI_UNSIGNED, MPI_UNSIGNED, MPI_UNSIGNED_LONG,
        MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG,
        MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG,
        MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG,
        MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG, MPI_UNSIGNED_LONG
    };

    int blocklens[30] =
    {
        256,1,
        1,1,1,1,
        1,1,1,1,1,1,
        1,1,1,1,1,1,
        1,1,1,1,1,1,
        1,1,1,1,1,1
    };

    MPI_Aint disp[30];
    MPI_Address(&runtime_config.filename, &disp[0]);
    MPI_Address(&runtime_config.debug, &disp[1]);

    MPI_Address(&runtime_config.time_latency, &disp[2]);
    MPI_Address(&runtime_config.client_number, &disp[3]);
    MPI_Address(&runtime_config.server_number, &disp[4]);
    MPI_Address(&runtime_config.stripe_size, &disp[5]);

    MPI_Address(&runtime_config.FOPEN_init, &disp[6]);
    MPI_Address(&runtime_config.FOPEN_terminate, &disp[7]);
    MPI_Address(&runtime_config.FOPEN_prelude, &disp[8]);
    MPI_Address(&runtime_config.FOPEN_positive_ack, &disp[9]);
    MPI_Address(&runtime_config.FOPEN_start_flow, &disp[10]);
    MPI_Address(&runtime_config.FOPEN_completion_ack, &disp[11]);

    MPI_Address(&runtime_config.FREAD_init, &disp[12]);
    MPI_Address(&runtime_config.FREAD_terminate, &disp[13]);
    MPI_Address(&runtime_config.FREAD_prelude, &disp[14]);
    MPI_Address(&runtime_config.FREAD_positive_ack, &disp[15]);
    MPI_Address(&runtime_config.FREAD_start_flow, &disp[16]);
    MPI_Address(&runtime_config.FREAD_completion_ack, &disp[17]);

    MPI_Address(&runtime_config.FWRITE_init, &disp[18]);
    MPI_Address(&runtime_config.FWRITE_terminate, &disp[19]);
    MPI_Address(&runtime_config.FWRITE_prelude, &disp[20]);
    MPI_Address(&runtime_config.FWRITE_positive_ack, &disp[21]);
    MPI_Address(&runtime_config.FWRITE_start_flow, &disp[22]);
    MPI_Address(&runtime_config.FWRITE_completion_ack, &disp[23]);

    MPI_Address(&runtime_config.FCLOSE_init, &disp[24]);
    MPI_Address(&runtime_config.FCLOSE_terminate, &disp[25]);
    MPI_Address(&runtime_config.FCLOSE_prelude, &disp[26]);
    MPI_Address(&runtime_config.FCLOSE_positive_ack, &disp[27]);
    MPI_Address(&runtime_config.FCLOSE_start_flow, &disp[28]);
    MPI_Address(&runtime_config.FCLOSE_completion_ack, &disp[29]);
    for(i=1;i<30;++i)
        disp[i] = disp[i] - disp[0];
    disp[0] = 0;

    MPI_Type_struct(30, blocklens, disp, types, &config_type);
    MPI_Type_commit(&config_type);

    MPI_Bcast(p_config, 1, config_type, g_tw_masternode, MPI_COMM_WORLD);

    MPI_Type_free(&config_type);

    return 0;
}
/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
