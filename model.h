#ifndef __hybrid_h__
#define __hybrid_h__

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ross.h>

typedef enum {
    FOPEN_init,
    FOPEN_prelude,
    FOPEN_positive_ack,
    FOPEN_start_flow,
    FOPEN_completion_ack,
    FOPEN_terminate,

    FREAD_init,
    FREAD_prelude,
    FREAD_positive_ack,
    FREAD_start_flow,
    FREAD_completion_ack,
    FREAD_terminate,

    FWRITE_init,
    FWRITE_prelude,
    FWRITE_positive_ack,
    FWRITE_start_flow,
    FWRITE_completion_ack,
    FWRITE_terminate,

    FCLOSE_init,
    FCLOSE_prelude,
    FCLOSE_positive_ack,
    FCLOSE_start_flow,
    FCLOSE_completion_ack,
    FCLOSE_terminate,

    HW_READ_START,
    HW_READ_READY,
    HW_READ_ACK,
    HW_READ_END,
    HW_WRITE_START,
    HW_WRITE_READY,
    HW_WRITE_ACK,
    HW_WRITE_END,
} message_type;

typedef struct {
    unsigned long id;
    message_type m_type;
    unsigned int file_handler;
    unsigned long long length;
    unsigned long long offset;
    tw_lpid sender_id;
    tw_lpid client_id;
    double start_time;
    double end_time;
} message;

typedef struct {
    long offset;
    int open_files;
    long requests;
    int sub_requests;
    unsigned long long total_request_size;
    double total_response_time;
} client_state;

typedef struct {
    unsigned long long offset;
    unsigned long long length;
    int open_files;
    int type;
} server_state;

typedef struct {
    unsigned long long position;
    unsigned long long capcity;
    int depth;
    unsigned short type; // 0 for HDD; 1 for Flash SSD;
    double next_avail_time;
} drive_state;

/* shared functions */
void printf_msg(const char *, const char *, const message *, const tw_lp *);
void send_msg(tw_lpid, message_type, unsigned int, tw_stime, tw_lp *, tw_lpid, unsigned long long, unsigned long long);
void strip_request(client_state *s, tw_lp * lp, message * msg);

/* statistic variables and functions */
unsigned long long hpis3_local_total_request_size;
double hpis3_local_total_response_time;

void hpis3_print_stat();
void hpis3_print_bw();

/* client functions */
void client_init(client_state*, tw_lp*);
void client_event_handler(client_state *, tw_bf *, message *, tw_lp *);
void client_revert_handler(client_state *, tw_bf *, message *, tw_lp *);
void client_final(client_state *, tw_lp *);
tw_peid client_mapping(tw_lpid);

/* server functions */
void server_init(server_state*, tw_lp*);
void server_event_handler(server_state *, tw_bf *, message *, tw_lp *);
void server_revert_handler(server_state *, tw_bf *, message *, tw_lp *);
void server_final(server_state *, tw_lp *);
tw_peid server_mapping(tw_lpid);

/* drive functions */
void drive_init(drive_state*, tw_lp*);
void drive_event_handler(drive_state *, tw_bf *, message *, tw_lp *);
void drive_revert_handler(drive_state *, tw_bf *, message *, tw_lp *);
void drive_final(drive_state *, tw_lp *);
tw_peid drive_mapping(tw_lpid);

#endif

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
