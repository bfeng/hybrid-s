#include "conf.h"
#include "model.h"

extern runtime_conf_t runtime_config;

/* functions for server model */

void server_init(server_state *s, tw_lp *lp)
{
    s->open_files = 0;
    if ( lp->gid >= (runtime_config.client_number + runtime_config.server_number / 2) )
        s->type = 1; // SSD
    else
        s->type = 0; // HDD
    if(runtime_config.debug)
    {
        printf("%s, Server #%u initilized on %u\n", s->type == 0 ? "HDD":"SSD", (unsigned int)lp->gid, (unsigned int)g_tw_mynode);
    }
}

void server_event_handler(server_state *s, tw_bf * bf, message * msg, tw_lp * lp)
{
    tw_event *e;
    message *m;

    switch(msg->m_type) {
        case FOPEN_prelude:
            {
                printf_msg("Server", "FOPEN_prelude", msg, lp);
                s->open_files++;
                send_msg(lp->gid, FOPEN_positive_ack, msg->file_handler, runtime_config.FOPEN_prelude, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FOPEN_positive_ack:
            {
                printf_msg("Server", "FOPEN_positive_ack", msg, lp);
                send_msg(lp->gid, FOPEN_start_flow, msg->file_handler, runtime_config.FOPEN_positive_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FOPEN_start_flow:
            {
                printf_msg("Server", "FOPEN_start_flow", msg, lp);
                send_msg(lp->gid, FOPEN_completion_ack, msg->file_handler, runtime_config.FOPEN_start_flow, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FOPEN_completion_ack:
            {
                printf_msg("Server", "FOPEN_completion_ack", msg, lp);
                send_msg(msg->client_id, FOPEN_terminate, msg->file_handler, runtime_config.FOPEN_completion_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FREAD_prelude:
            {
                printf_msg("Server", "FREAD_prelude", msg, lp);
                send_msg(lp->gid, FREAD_positive_ack, msg->file_handler, runtime_config.FREAD_prelude, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FREAD_positive_ack:
            {
                printf_msg("Server", "FREAD_positive_ack", msg, lp);
                send_msg(lp->gid, FREAD_start_flow, msg->file_handler, runtime_config.FREAD_positive_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FREAD_start_flow:
            {
                printf_msg("Server", "FREAD_start_flow", msg, lp);
                send_msg(lp->gid, FREAD_completion_ack, msg->file_handler, runtime_config.FREAD_start_flow, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FREAD_completion_ack:
            {
                printf_msg("Server", "FREAD_completion_ack", msg, lp);
                send_msg(msg->client_id, FREAD_terminate, msg->file_handler, runtime_config.FREAD_completion_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FWRITE_prelude:
            {
                printf_msg("Server", "FWRITE_prelude", msg, lp);
                send_msg(lp->gid, FWRITE_positive_ack, msg->file_handler, runtime_config.FWRITE_prelude, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FWRITE_positive_ack:
            {
                printf_msg("Server", "FWRITE_positive_ack", msg, lp);
                send_msg(lp->gid, FWRITE_start_flow, msg->file_handler, runtime_config.FWRITE_positive_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FWRITE_start_flow:
            {
                printf_msg("Server", "FWRITE_start_flow", msg, lp);
                send_msg(lp->gid, FWRITE_completion_ack, msg->file_handler, runtime_config.FWRITE_start_flow, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FWRITE_completion_ack:
            {
                printf_msg("Server", "FWRITE_completion_ack", msg, lp);
                send_msg(msg->client_id, FWRITE_terminate, msg->file_handler, runtime_config.FWRITE_completion_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FCLOSE_prelude:
            {
                printf_msg("Server", "FCLOSE_prelude", msg, lp);
                s->open_files--;
                send_msg(lp->gid, FCLOSE_positive_ack, msg->file_handler, runtime_config.FCLOSE_prelude, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FCLOSE_positive_ack:
            {
                printf_msg("Server", "FCLOSE_positive_ack", msg, lp);
                send_msg(lp->gid, FCLOSE_start_flow, msg->file_handler, runtime_config.FCLOSE_positive_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FCLOSE_start_flow:
            {
                printf_msg("Server", "FCLOSE_start_flow", msg, lp);
                send_msg(lp->gid, FCLOSE_completion_ack, msg->file_handler, runtime_config.FCLOSE_start_flow, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
        case FCLOSE_completion_ack:
            {
                printf_msg("Server", "FCLOSE_completion_ack", msg, lp);
                send_msg(msg->client_id, FCLOSE_terminate, msg->file_handler, runtime_config.FCLOSE_completion_ack, lp, msg->client_id, msg->offset, msg->length);
                break;
            }
    }
}

void server_revert_handler(server_state *s, tw_bf * bf, message * msg, tw_lp * lp)
{
}

void server_final(server_state *s, tw_lp *lp)
{
    if(runtime_config.debug)
        printf("server#%u: open files: %d\n", (unsigned int) lp->gid, s->open_files);
}

tw_peid server_mapping(tw_lpid gid)
{
    return (tw_peid) gid / g_tw_nlp;
}

void drive_init(drive_state *s, tw_lp *lp)
{
    if(runtime_config.debug)
        printf("Drive #%u initilized on %u\n", (unsigned int) lp->gid, (unsigned int)g_tw_mynode);
}

void drive_start(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp, message_type type)
{
    tw_stime ts;
    tw_event *e;
    message *m;

    switch(s->type) {
        case 0: // HDD
            ts = 100;
            break;
        case 1: // SSD
            ts = 10;
            break;
    }

    s->next_avail_time = fmax(s->next_avail_time, tw_now(lp));
    e = tw_event_new(msg->sender_id, ts + s->next_avail_time - tw_now(lp), lp);
    m = tw_event_data(e);
    m->m_type = HW_READ_READY;
    m->id = msg->id;
    m->sender_id = lp->gid;

    s->next_avail_time += ts;
    tw_event_send(e);
}

void drive_ready(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp, message_type type)
{
    tw_stime ts;
    tw_event *e;
    message *m;

    switch(s->type) {
        case 0: // HDD
            ts = 100;
            break;
        case 1: // SSD
            ts = 10;
            break;
    }

    s->next_avail_time = fmax(s->next_avail_time, tw_now(lp));
    e = tw_event_new(msg->sender_id, ts + s->next_avail_time - tw_now(lp), lp);
    m = tw_event_data(e);
    m->m_type = HW_READ_ACK;
    m->id = msg->id;
    m->sender_id = lp->gid;

    s->next_avail_time += ts;
    tw_event_send(e);
}

void drive_ack(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp, message_type type)
{
    tw_stime ts;
    tw_event *e;
    message *m;

    switch(s->type) {
        case 0: // HDD
            ts = 100;
            break;
        case 1: // SSD
            ts = 10;
            break;
    }

    s->next_avail_time = fmax(s->next_avail_time, tw_now(lp));
    e = tw_event_new(msg->sender_id, ts + s->next_avail_time - tw_now(lp), lp);
    m = tw_event_data(e);
    m->m_type = HW_READ_END;
    m->id = msg->id;
    m->sender_id = lp->gid;

    s->next_avail_time += ts;
    tw_event_send(e);
}

void drive_end(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp, message_type type)
{
    tw_stime ts;
    tw_event *e;
    message *m;

    switch(s->type) {
        case 0: // HDD
            ts = 100;
            break;
        case 1: // SSD
            ts = 10;
            break;
    }

    s->next_avail_time = fmax(s->next_avail_time, tw_now(lp));
    e = tw_event_new(msg->sender_id, ts + s->next_avail_time - tw_now(lp), lp);
    m = tw_event_data(e);
    m->m_type = HW_READ_END;
    m->id = msg->id;
    m->sender_id = lp->gid;

    s->next_avail_time += ts;
    tw_event_send(e);
}

void drive_event_handler(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp)
{
    switch(msg->m_type)
    {
        case HW_READ_START:
        case HW_WRITE_START:
            drive_start(s, bf, msg, lp, msg->m_type);
            break;
        case HW_READ_READY:
        case HW_WRITE_READY:
            drive_ready(s, bf, msg, lp, msg->m_type);
            break;
        case HW_READ_ACK:
        case HW_WRITE_ACK:
            drive_ack(s, bf, msg, lp, msg->m_type);
            break;
        case HW_READ_END:
        case HW_WRITE_END:
            drive_end(s, bf, msg, lp, msg->m_type);
            break;
    }
}

void drive_revert_handler(drive_state * s, tw_bf * bf, message * msg, tw_lp * lp)
{
}

void drive_final(drive_state * s, tw_lp * lp)
{
}

tw_peid drive_mapping(tw_lpid gid)
{
    return (tw_peid) gid / g_tw_nlp;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
