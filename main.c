#include <stdio.h>
#include <stdbool.h>
#include <ross.h>
#include "conf.h"
#include "model.h"

extern runtime_conf_t runtime_config;
extern unsigned long long hpis3_local_total_request_size;
extern double hpis3_local_total_response_time;

const tw_optdef app_opts[] =
{
    TWOPT_GROUP("Hybrid PVFS Model"),
    TWOPT_CHAR("config", runtime_config.filename, "configuration file"),
    TWOPT_END()
};

tw_lptype model_lps[] =
{
    {
        (init_f) client_init,
        (pre_run_f) NULL,
        (event_f) client_event_handler,
        (revent_f) client_revert_handler,
        (final_f) client_final,
        (map_f) client_mapping,
        sizeof(client_state),
    },
    {
        (init_f) server_init,
        (pre_run_f) NULL,
        (event_f) server_event_handler,
        (revent_f) server_revert_handler,
        (final_f) server_final,
        (map_f) server_mapping,
        sizeof(server_state),
    },
    {
        (init_f) drive_init,
        (pre_run_f) NULL,
        (event_f) drive_event_handler,
        (revent_f) drive_revert_handler,
        (final_f) drive_final,
        (map_f) drive_mapping,
        sizeof(drive_state),
    },
    {0}
};

int simulator_init()
{
    if(runtime_config.filename && sizeof(runtime_config.filename) > 0)
    {
        if(load_configuration_file(&runtime_config)==-1)
            return -1;
    }
    else
        return -1;

    hpis3_local_total_request_size = 0;
    hpis3_local_total_response_time = 0;

    return 0;
}

int simulation_main(int argc, char ** argv)
{
    int i, result;
    int num_lps_per_pe = 2;

    /* initialize */
    tw_opt_add(app_opts);
    tw_init(&argc, &argv);

    result = simulator_init();
    if (result != 0)
        return result;

    /* define lps */
    num_lps_per_pe = (runtime_config.client_number + runtime_config.server_number * 2) / (tw_nnodes() * g_tw_npe);
    /*g_tw_events_per_pe = 15000000;*/
    /*g_tw_lookahead = 200;*/
    /*g_tw_mapping = ROUND_ROBIN;*/
    /*g_tw_mapping = CUSTOM;*/

    tw_define_lps(num_lps_per_pe, sizeof(message), 0);

    if(runtime_config.debug)
        printf("g_tw_nlp:%d\n", (int)g_tw_nlp);

    for (i = 0; i < g_tw_nlp; ++i)
    {
        /* define lp in linear way */
        if ( (i + g_tw_mynode*num_lps_per_pe) < runtime_config.client_number )
            tw_lp_settype(i, &model_lps[0]);
        else if ( (i + g_tw_mynode*num_lps_per_pe) < (runtime_config.client_number + runtime_config.server_number) )
            tw_lp_settype(i, &model_lps[1]);
        else
            tw_lp_settype(i, &model_lps[2]);
        /* define lp in round-robin way */
        /*if ( i < (runtime_config.client_number / (tw_nnodes() * g_tw_npe)) )*/
        /*tw_lp_settype(i, &model_lps[0]);*/
        /*else if ( i < (runtime_config.client_number / (tw_nnodes() * g_tw_npe))*/
        /*+ (runtime_config.server_number / (tw_nnodes() * g_tw_npe)) )*/
        /*tw_lp_settype(i, &model_lps[1]);*/
        /*else*/
        /*tw_lp_settype(i, &model_lps[2]);*/
    }

    tw_run();

    /*
     * print out the statistics from simulation
     */
    hpis3_print_stat();

    tw_end();

    return 0;
}

int main(int argc, char ** argv)
{
    return simulation_main(argc, argv);
}
/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ft=c ts=8 sts=4 sw=4 expandtab
 */
